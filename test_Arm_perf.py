import time
from unittest import TestCase
import call_mxAoiService as aoi


class ArmPerf(TestCase):
    def setUp(self) -> None:
        self.SERVER_URL = 'http://123.60.231.100:8895/infer'
        self.TASK_NAME = 'assm1-2'

        self.model_pro_dic = {
            'ssd': 'project_ssd',
            'ssd+resnet': 'project_ssd_resnet',
            'yolov4': 'project_yolov4',
            'yolov4+resnet': 'project_yolov4_resnet',
            'unet': 'project_unet',
            'unet++': 'project_unet++',
            'crnn': 'project_crnn',
            'ctpn_crnn': 'project_ctpn_crnn',
            'yolov4+resnet_ch': 'project_yolov4_resnet_ch',
        }
        aoi.model_pro_dic = self.model_pro_dic

    def test_perf_ssd_1000(self):
        for i in range(1000):
            print(f'ssd times: {i + 1}')
            self.test_ssd()

    def test_perf_ssd_resnet_1000(self):
        for i in range(1000):
            print(f'ssd_resnet times: {i + 1}')
            self.test_ssd_resnet()

    def test_perf_yolov4_1000(self):
        for i in range(1000):
            print(f'yolov4 times: {i + 1}')
            self.test_yolov4()

    def test_perf_yolov4_resnet_1000(self):
        for i in range(1000):
            print(f'yolov4_resnet times: {i + 1}')
            self.test_yolov4_resnet()

    def test_perf_crnn_5000(self):
        for i in range(5000):
            print(f'crnn times: {i + 1}')
            self.test_crnn()
            time.sleep(0.8)

    def test_perf_ctpn_crnn_5000(self):
        for i in range(5000):
            print(f'ctpn_crnn times: {i + 1}')
            self.test_ctpn_crnn()
            time.sleep(0.8)

    def test_ssd(self):
        img_path = 'test_imgs/ssd.jpg'
        code, rsp = aoi.run_one_model('ssd',
                                      task_name=self.TASK_NAME,
                                      service_addr=self.SERVER_URL,
                                      img_path=img_path, need_reg=True)
        self.assertEqual(code, 200, '返回码不是200')
        self.assertTrue(rsp.get(img_path).get('Objects'), '推理结果为空')
        self.assertTrue(rsp.get(img_path).get('Objects')[0].get("Box"), '推理结果不正确,无box')

    def test_ssd_resnet(self):
        img_path = 'test_imgs/ssd+resnet.jpg'
        code, rsp = aoi.run_one_model('ssd+resnet',
                                      task_name=self.TASK_NAME,
                                      service_addr=self.SERVER_URL,
                                      img_path=img_path, need_reg=True)
        self.assertEqual(code, 200, '返回码不是200')
        self.assertTrue(rsp.get(img_path).get('Objects'), '推理结果为空')
        self.assertTrue(rsp.get(img_path).get('Objects')[0].get("Box"), '推理结果不正确,无box')
        self.assertTrue(rsp.get(img_path).get('Objects')[0].get("Classification"), '推理结果不正确,无Classification')

    def test_yolov4(self):
        img_path = 'test_imgs/baode.jpg'
        code, rsp = aoi.run_one_model('yolov4',
                                      task_name=self.TASK_NAME,
                                      service_addr=self.SERVER_URL,
                                      img_path=img_path, need_reg=True)
        self.assertEqual(code, 200, '返回码不是200')
        # self.assertTrue(rsp.get(img_path).get('Objects'), '推理结果为空')

    def test_yolov4_resnet(self):
        img_path = 'test_imgs/200dk.jpg'
        code, rsp = aoi.run_one_model('yolov4+resnet',
                                      task_name=self.TASK_NAME,
                                      service_addr=self.SERVER_URL,
                                      img_path=img_path, need_reg=True)
        self.assertEqual(code, 200, '返回码不是200')
        self.assertTrue(rsp.get(img_path).get('Objects'), '推理结果为空')

    def test_unet(self):
        img_path = 'test_imgs/unet.jpg'
        code, rsp = aoi.run_one_model('unet',
                                      task_name=self.TASK_NAME,
                                      service_addr=self.SERVER_URL,
                                      img_path=img_path, need_reg=True)
        self.assertEqual(code, 200, '返回码不是200')
        self.assertTrue(rsp.get(img_path).get('BlobContours'), '推理结果为空')
        self.assertTrue(rsp.get(img_path).gKet('LabelMap'), '推理结果不正确,无LabelMap')

    def test_unetpp(self):
        img_path = 'test_imgs/unet++.jpg'
        code, rsp = aoi.run_one_model('unet++',
                                      task_name=self.TASK_NAME,
                                      service_addr=self.SERVER_URL,
                                      img_path=img_path, need_reg=True)
        self.assertEqual(code, 200, '返回码不是200')
        self.assertTrue(rsp.get(img_path).get('BlobContours'), '推理结果为空')
        self.assertTrue(rsp.get(img_path).get('LabelMap'), '推理结果不正确,无LabelMap')

    def test_crnn(self):
        img_path = 'test_imgs/crnn.jpg'
        code, rsp = aoi.run_one_model('crnn',
                                      task_name=self.TASK_NAME,
                                      service_addr=self.SERVER_URL,
                                      img_path=img_path, need_reg=True)
        self.assertEqual(code, 200, '返回码不是200')
        self.assertTrue(rsp.get(img_path).get('Texts'), '推理结果为空')
        self.assertEqual(rsp.get(img_path).get('Texts')[0].get("OCRText"), 'KYJJ', '推理结果不正确')

    def test_ctpn_crnn(self):
        img_path = 'test_imgs/disk.jpg'
        code, rsp = aoi.run_one_model('ctpn_crnn',
                                      task_name=self.TASK_NAME,
                                      service_addr=self.SERVER_URL,
                                      img_path=img_path, need_reg=True)
        self.assertEqual(code, 200, '返回码不是200')
        self.assertTrue(rsp.get(img_path).get('Texts'), '推理结果为空')
