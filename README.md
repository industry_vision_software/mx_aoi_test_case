# 半自动化测试用例使用说明

1. 安装python依赖:
pip install -r py_requirements.txt

2. 如果模型有更新
   1. 将图片放到reqs目录下
   2. 修改 test_x86_YiTong.py 对应用例的 img_path,server_url,project_name,task_name
   3. 根据如下情况,执行对应的脚本

## 功能测试

* 全量测试: python -m unittest test_x86_YiTong

* 测试ssd: python -m unittest test_x86_YiTong.X86_210626.test_ssd

* 测试yolov4: python -m unittest test_x86_YiTong.X86_210626.test_yolov4

* 测试ssd+resnet: python -m unittest test_x86_YiTong.X86_210626.test_ssd_resnet

* 测试yolov4+resnet: python -m unittest test_x86_YiTong.X86_210626.test_yolov4_resnet

* 测试crnn: python -m unittest test_x86_YiTong.X86_210626.test_crnn

* 测试ctpn+crnn: python -m unittest test_x86_YiTong.X86_210626.test_ctpn_crnn

* 测试unet: python -m unittest test_x86_YiTong.X86_210626.test_unet

* 测试unet++: python -m unittest test_x86_YiTong.X86_210626.test_unetpp

## 性能测试

### 深圳蓝区

* ssd 10次: python -m unittest test_x86_sz_perf.SzPerf.test_ssd
* ssd 1000次: python -m unittest test_x86_sz_perf.SzPerf.test_ssd_1000
