#!/usr/bin/env python
# _*_coding:utf-8_*_
import json
import os

import cv2


def draw_patterns():
    image_path = r'E:\atlas\opt\pics\200dk\200dk_grey.jpg'
    # with open('req_rsp_sample/det-rsp.json', 'rb') as f:
    with open('req_rsp_sample/det-rsp.json', 'rb') as f:
        req_dict = json.load(f)
    img = cv2.imread(image_path)
    patterns = req_dict.get('200dk_grey.jpg').get('Patterns')
    if patterns:
        for pattern in patterns:
            # W = int(pattern.get('Width'))
            # H = int(pattern.get('Height'))
            # X = int(pattern.get('X'))
            # Y = int(pattern.get('Y'))
            X, Y, W, H = 461, 425, 360, 283
            pt1 = (X, Y)
            pt2 = (X + W, Y + H)
            print('{0},{1},{2},{3}'.format(X, Y, W, H))
        cv2.rectangle(img, pt1, pt2, (0, 255, 0), 2)
    res_path = 'res_img'
    if not os.path.exists(res_path):
        os.makedirs(res_path)
    cv2.imwrite(os.path.join(res_path, 'res.jpg'), img)
    print('=== end ===')


if __name__ == '__main__':
    draw_patterns()
