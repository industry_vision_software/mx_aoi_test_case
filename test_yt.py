from unittest import TestCase
import call_mxAoiService as aoi


class X86_210626Yitong(TestCase):
    def setUp(self) -> None:
        self.SERVER_URL = 'https://123.60.231.101:80/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLTg2ZGUyOGU5LTgwMDItNGRhZi04MGJhLTVhMmUwZTg2MmQyMS5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        self.TASK_NAME = 'opt_crnn_test'
        self.model_pro_dic = {
            'ssd': 'project_ssd',
            'ssd+resnet': 'project_ssd_resnet',
            'yolov4': 'project_yolov4',
            'yolov4+resnet': 'project_yolov4_resnet',
            'unet': 'project_unet',
            'unet++': 'project_unet++',
            'crnn': 'cran-0703-1',
            'ctpn_crnn': 'project_ctpn_crnn',
            'yolov4+resnet_ch': 'project_yolov4_resnet_ch',
        }
        aoi.model_pro_dic = self.model_pro_dic
        #设置证书路径
        aoi.CA_DIR='E:/nextcloud_lpf/atlas_config/keys/101_k8s'

    def test_ssd(self):
        img_path = 'test_imgs/200dk.jpg'
        server_url = 'https://123.60.231.101:443/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLWIxNzJjZjdjLWU2NTQtNGZmZi1iMzZkLTYxODU3YjI5ZDcxNy5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        code, rsp = aoi.run_one_model('ssd',
                                      task_name='test3',
                                      project_name='ssd_dataset',
                                      service_addr=server_url,
                                      img_path=img_path,
                                      need_reg=False)
        self.assertEqual(code, 200, 'ssd 返回码不是200')
        self.assertTrue(rsp.get(img_path).get('Objects'), 'ssd 推理结果为空')
        self.assertTrue(rsp.get(img_path).get('Objects')[0].get("Box"), 'ssd 推理结果不正确,无box')

    def test_ssd_resnet(self):
        img_path = 'test_imgs/200dk_grey.jpg'
        server_url = 'https://123.60.231.101:443/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLTBlYzA1ODdkLTUzZWMtNGJlNS04YzE3LWE0N2Y4Y2ZkMmVkYi5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        code, rsp = aoi.run_one_model('ssd+resnet',
                                      task_name='ssd_resnet50_1026',
                                      project_name='ssd_resnet50_1026',
                                      service_addr=server_url,
                                      img_path=img_path,
                                      need_reg=True)
        self.assertEqual(code, 200, 'ssd_resnet 返回码不是200')
        self.assertTrue(rsp.get(img_path).get('Objects'), 'ssd_resnet 推理结果为空')
        self.assertTrue(rsp.get(img_path).get('Objects')[0].get("Box"), 'ssd_resnet 推理结果不正确,无box')
        has_cls = False
        for box in rsp.get(img_path).get('Objects'):
            cls = box.get("Classification")
            if cls:
                has_cls = True
                break
        self.assertTrue(has_cls, 'ssd_resnet 推理结果不正确,无Classification')

    def test_yolov4(self):
        img_path = 'test_imgs/200dk.jpg'
        server_url = 'https://123.60.231.101:443/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLTIyMjkxNGJkLTY3YzEtNDc0ZC05ZmVjLWFhYmVjZTZhZGQ4Ni5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        code, rsp = aoi.run_one_model('yolov4',
                                      task_name='yolov4-0924',
                                      project_name='yolov4-0922',
                                      service_addr=server_url,
                                      img_path=img_path,
                                      need_reg=False)
        self.assertEqual(code, 200, 'yolov4 返回码不是200')
        self.assertTrue(rsp.get(img_path).get('Objects'), 'yolov4 推理结果为空')

    def test_yolov4_resnet(self):
        img_path = 'test_imgs/yolov4_resnet-yitong.jpg'
        server_url = 'https://123.60.231.101:80/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLTUwMTQ0ZjhjLTFhZmUtNDlmOS1iZWM5LTBhZGY0NmZlZjg0NC5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        code, rsp = aoi.run_one_model('yolov4+resnet',
                                      project_name='yolov4_resnet50_0709-2',
                                      task_name='huawei_yolov4_resnet50_0710',
                                      service_addr=server_url,
                                      img_path=img_path,
                                      need_reg=True)
        self.assertEqual(code, 200, 'yolov4_resnet 返回码不是200')
        has_cls = False
        for box in rsp.get(img_path).get('Objects'):
            cls = box.get("Classification")
            if cls:
                has_cls = True
                break
        self.assertTrue(has_cls, 'ssd_resnet 推理结果不正确,无Classification')

    def test_crnn(self):
        img_path = 'test_imgs/ctpn-yt.jpg'
        server_url = 'https://123.60.231.101:443/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLTk4MTY2ZGRmLTgwNTMtNDQ0Yi04M2M0LThlNzA2MzA1ZTQ0MC5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        code, rsp = aoi.run_one_model('crnn',
                                      task_name='opt_ocr',
                                      project_name='ocr_0923',
                                      service_addr=server_url,
                                      img_path=img_path,
                                      need_reg=True)
        self.assertEqual(code, 200, 'crnn 返回码不是200')
        self.assertTrue(rsp.get(img_path).get('Texts'), 'crnn 推理结果为空')
        self.assertEqual(rsp.get(img_path).get('Texts')[0].get("OCRText"), 'Textbookof', 'crnn 推理结果不正确')

    def test_ctpn_crnn(self):
        img_path = 'test_imgs/ctpn-yt.jpg'
        server_url = 'https://123.60.231.101:443/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLTk4MTY2ZGRmLTgwNTMtNDQ0Yi04M2M0LThlNzA2MzA1ZTQ0MC5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        code, rsp = aoi.run_one_model('ctpn_crnn',
                                      task_name='opt_ocr',
                                      project_name='ocr_0923',
                                      service_addr=server_url,
                                      img_path=img_path,
                                      need_reg=True)
        self.assertEqual(code, 200, 'ctpn_crnn 返回码不是200')
        self.assertTrue(rsp.get(img_path).get('Texts'), 'ctpn_crnn 推理结果为空')

    def test_unet_tpl(self):
        img_path = 'test_imgs/chip.jpg'
        server_url = 'https://123.60.231.101:443/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLWQ1MmZmZDE4LWVhMjctNDMwNi04YmY1LTMxOWM4OWFlMDc1Yi5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        code, rsp = aoi.run_unet('unet',
                                 task_name='unet_1013',
                                 project_name='unet_1011',
                                 service_addr=server_url,
                                 img_path=img_path,
                                 need_reg=True,
                                 cameraId='c61')
        self.assertEqual(code, 200, 'unet  返回码不是200')
        self.assertTrue(rsp.get(img_path).get('BlobContours'), 'unet  推理结果为空')
        self.assertTrue(rsp.get(img_path).get('LabelMap'), 'unet  推理结果不正确,无LabelMap')

    def test_unet(self):
        img_path = 'test_imgs/unet-yt.jpg'
        server_url = 'https://123.60.231.101:80/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLTc1MWYxNjgxLWUyNDQtNDVlYy1hNGQ2LTdlZmRlZjU1NTdjOC5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        code, rsp = aoi.run_one_model('unet',
                                      project_name='unet-chenhang0712-1',
                                      task_name='tmp_unet0714',
                                      service_addr=server_url,
                                      img_path=img_path,
                                      need_reg=True,
                                      cameraId='c61')
        self.assertEqual(code, 200, 'unet  返回码不是200')
        self.assertTrue(rsp.get(img_path).get('BlobContours'), 'unet  推理结果为空')
        self.assertTrue(rsp.get(img_path).get('LabelMap'), 'unet  推理结果不正确,无LabelMap')

    def test_unetpp_tpl(self):
        img_path = 'test_imgs/chip.jpg'
        server_url = 'https://123.60.231.101:80/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLTBmZDU1Y2JhLWJmNzYtNGJjZC1hNzJkLWYxYzRlY2M1MmMyOS5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        code, rsp = aoi.run_unet('unet++',
                                 task_name='sample_unetpp_0730_2',
                                 project_name='unet_nested_0731',
                                 service_addr=server_url,
                                 img_path=img_path,
                                 need_reg=True,
                                 cameraId='c63')
        self.assertEqual(code, 200, 'unet++ 返回码不是200')
        self.assertTrue(rsp.get(img_path).get('BlobContours'), 'unet++ 推理结果为空')
        self.assertTrue(rsp.get(img_path).get('LabelMap'), 'unet++ 推理结果不正确,无LabelMap')

    def test_unetpp(self):
        img_path = 'test_imgs/chip.jpg'
        server_url = 'https://123.60.231.101:80/endpoints/inference/eyJzZXJ2aWNlIjoiaW5mZXJlbmNlLWluZmVyLTU2NzRjZDkwLWEwZDctNDc5YS05YTZhLTIxMjI3MWY3Y2RlYi5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwicG9ydCI6Ijg4ODgifQ==/infer'
        code, rsp = aoi.run_one_model('unet++',
                                      project_name='unetpp_chenhang0714-1',
                                      task_name='tmp_unetpp_07142',
                                      service_addr=server_url,
                                      img_path=img_path,
                                      need_reg=True,
                                      cameraId='c63')
        self.assertEqual(code, 200, 'unet++ 返回码不是200')
        self.assertTrue(rsp.get(img_path).get('BlobContours'), 'unet++ 推理结果为空')
        self.assertTrue(rsp.get(img_path).get('LabelMap'), 'unet++ 推理结果不正确,无LabelMap')
